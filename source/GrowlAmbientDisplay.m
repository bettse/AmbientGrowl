//
//  GrowlAmbientDisplay.h
//  Growl Display Plugins
//
//  Copyright 2006-2009 The Growl Project. All rights reserved.
//

#import "GrowlAmbientDisplay.h"
@implementation GrowlAmbientDisplay

- (id) init {
	self = [super init];
	return self;
}

- (void) dealloc {
	[preferencePane release];
	[super dealloc];
}

- (NSPreferencePane *) preferencePane {
    if (!preferencePane)
        preferencePane = [[GrowlAmbientPrefs alloc] initWithBundle:[NSBundle bundleForClass:[GrowlAmbientPrefs class]]];
    return preferencePane;
}

- (void) configureBridge:(GrowlNotificationDisplayBridge *)theBridge {
	//Save bridge for use in sending notifications to other Growl displays
	lastBridge = theBridge;
}


#pragma mark -
#pragma mark GrowlPositionController Methods
#pragma mark -

- (BOOL)requiresPositioning {
	return NO;
}


#pragma mark -
#pragma mark AmbientNotify Protocol
#pragma	mark -

/* The screensaver receiving a string using a distributed object and that 
 * string is the name of the application that is making the notification.
 * The application name is hashed to determine the color of the notification.
 * If a notification for that application already exists, the size and speed
 * of the notificaiton object are increased linearly.
 */
@protocol AmbientNotify
- (oneway void) request:(NSString*)request;
@end 

#pragma mark -
#pragma mark displayNotification
#pragma	mark -

- (void) displayNotification:(GrowlApplicationNotification *)notification {
    id proxy;
    NSConnection *connection;
    NSString * appName = [notification applicationName];
	NSString * AlternateStyle = nil;
	


	//At some later time, support determining if screensaver is active and save the trouble of sending the notification if it isn't
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionDidDie:) name:NSConnectionDidDieNotification object:connection];

	//Get a connection to the screensaver
    connection = [NSConnection connectionWithRegisteredName:AmbientPrefDomain host:nil];
    if(connection){
		//Get the proxy object (Its sorta like having a copy of the AmbientNotifyView object locally)
        proxy = [[connection rootProxy] retain];
        if(proxy){
            [proxy setProtocolForProxy:@protocol(AmbientNotify)];
            [proxy request:appName];
            NSLog(@"%s request sent", __PRETTY_FUNCTION__);
        }else{
            NSLog(@"%s request not sent", __PRETTY_FUNCTION__); 
        }
    }else{
		READ_GROWL_PREF_VALUE(Ambient_ALTDISPLAY_PREF, AmbientPrefDomain, NSString *, &AlternateStyle);
		if (AlternateStyle && [AlternateStyle isKindOfClass:[NSString class]]){		
			GrowlDisplayPlugin *plugin = (GrowlDisplayPlugin *)[[GrowlPluginController sharedController] displayPluginInstanceWithName:AlternateStyle
																																author:nil
																															   version:nil
																																  type:nil];
			if (lastBridge) {
				[plugin configureBridge:lastBridge];
			}
			[plugin displayNotification:notification];
			NSLog(@"%s using AlternateStyle %@", __PRETTY_FUNCTION__, AlternateStyle);
			
		}
	}
     
}



@end
