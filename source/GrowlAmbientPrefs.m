//
//  GrowlAmbientPrefs.m
//  Display Plugins
//
//  Copyright 2006-2009 The Growl Project. All rights reserved.
//

#import "GrowlAmbientPrefs.h"

@implementation GrowlAmbientPrefs

- (NSString *) mainNibName {
	return @"GrowlAmbientPrefs";
}

- (void) awakeFromNib {
	[self setDisplayPlugins:[[[GrowlPluginController sharedController] displayPlugins] valueForKey:GrowlPluginInfoKeyName]];

}
- (void) mainViewDidLoad {
}

- (void) didSelect {
	SYNCHRONIZE_GROWL_PREFS();
}

- (NSString *) alternateDisplay {
	NSString* value = nil;
	READ_GROWL_PREF_VALUE(Ambient_ALTDISPLAY_PREF, AmbientPrefDomain, NSString *, &value);
	if (value && [value isKindOfClass:[NSString class]]){
		NSLog(@"%s returning %@",__PRETTY_FUNCTION__, value);
		return value;
	}else{
		return nil;
	}
}

- (void) setAlternateDisplay:(NSString *)value {
	NSLog(@"%s setting %@",__PRETTY_FUNCTION__, value);
	WRITE_GROWL_PREF_VALUE(Ambient_ALTDISPLAY_PREF, value, AmbientPrefDomain);
	UPDATE_GROWL_PREFS();
}

- (NSArray *) displayPlugins {
    return plugins;
}

- (void) setDisplayPlugins:(NSArray *)thePlugins {
    if (thePlugins != plugins) {
        [plugins release];
        plugins = [thePlugins retain];
    }    
}

@end
