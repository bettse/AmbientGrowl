//
//  GrowlAmbientPrefs.h
//  Display Plugins
//
//  Copyright 2006-2009 The Growl Project. All rights reserved.
//

#import <PreferencePanes/PreferencePanes.h>
#import "GrowlDefinesInternal.h"
#import <GrowlPluginController.h>


#define AmbientPrefDomain			@"org.ericbetts.ambientgrowl"
#define Ambient_ALTDISPLAY_PREF         @"alternateDisplay"


@interface GrowlAmbientPrefs : NSPreferencePane {
	NSArray                         *plugins;

}

- (NSString *) alternateDisplay;
- (void) setAlternateDisplay:(NSString *)value;
- (NSArray *) displayPlugins;
- (void) setDisplayPlugins:(NSArray *)thePlugins;


@end
