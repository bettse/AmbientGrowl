//
//  GrowlAmbientDisplay.h
//  Growl Display Plugins
//
//  Copyright 2006-2009 The Growl Project. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <GrowlDisplayPlugin.h>
#import "GrowlAmbientPrefs.h"
#import "GrowlApplicationNotification.h"
#import <GrowlDefinesInternal.h>
#import <GrowlDefines.h>
#import <GrowlPluginController.h>

@class GrowlApplicationNotification;

@interface GrowlAmbientDisplay : GrowlDisplayPlugin {

	GrowlNotificationDisplayBridge* lastBridge;
}


@end
